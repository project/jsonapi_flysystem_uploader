<?php
  
namespace Drupal\jsonapi_flysystem_uploader\Plugin\QueueWorker;

/**
 * Push entity data to a remote endpoint when triggered via entity update.
 *
 * @QueueWorker(
 *   id = "jsonapi_flysystem_uploader_push",
 *   title = @Translation("Cron task for JSON API Flysystem Upload pusher"),
 *   cron = {"time" = 10}
 * )
 */
class CronDataPush extends DataPushBase {}
