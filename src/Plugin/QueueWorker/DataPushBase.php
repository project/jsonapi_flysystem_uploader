<?php

namespace Drupal\jsonapi_flysystem_uploader\Plugin\QueueWorker;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Entity\EntityInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\Entity\EntityManagerInterface;

/**
 * Provides base functionality for the DataPush Queue Workers.
 */
abstract class DataPushBase extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;

  /**
   * Creates a new DataPushBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   Entity manager service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityManagerInterface $entity_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityManager = $entity_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    /** @var EntityInterface $entity */
    $entity = $this->entityManager
      ->getStorage($data->entity_type_id)
      ->load($data->entity_id);
    return $this->pushFile($entity, $data->config);
  }

  /**
   * Publishes a node.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to be processed.
   * @param array $config
   *   The configuration array as generated by the entity hook.
   *
   * @return int
   */
  protected function pushFile(EntityInterface $entity, array $config) {
    // For a given $entity object.
    // @todo Change this to use dependency injection.
    $output = \Drupal::service('jsonapi_extras.entity.to_jsonapi')->serialize($entity);
    $entity_type_id = $entity->getEntityType()->id();
    $bundle_id = $entity->bundle();
    $uuid = $entity->uuid();
    $filename = $uuid . '.json';

    // Load the appropriate configuration.
    $config = $this->getEndpointConfig($entity);

    // The Flysystem endpoint to use.
    $endpoint = $config['flysystem'];

    // The composite object will be stored in a specific directory.
    $directory = $bundle_id;

    // Optional base path.
    if (!empty($config['base path'])) {
      $directory = trim($config['base path'], '/') . '/' . $directory;
    }

    // Compile the full endpoint path.
    $path = $endpoint . '://' . $directory . '/' . $filename;

    // Make the path more system friendly.
    $path = file_stream_wrapper_uri_normalize($path);

    // Upload the file.
    file_put_contents($path, $output);
    $message = 'Pushed ' . $path . ' via Flysystem.';
    \Drupal::logger('jsonapi_flysystem_uploader')->notice($message);

    // Close the connection.
    return 1;
  }

  /**
   * Obtain the config array for a specific entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity that needs its configuration obtained.
   *
   * @return array
   *   One value must be present in the array:
   *   * flysystem - The name of the Flysystem configuration that is to be used.
   *     These must be defined in the normal Flysystem settings.
   *     @see $settings['flysystem']
   *   One other value may be present:
   *   * directory - All files will be uploaded to the specified directory. If
   *     this is not available then the file will be stored in a directory with
   *     the same name as the bundle. Leading or trailing slashes are not needed
   *     and will be ignored.
   */
  protected function getEndpointConfig($entity) {
    // Load the settings for this object.
    $settings = Settings::get('jsonapi_flysystem_uploader');

    // Look for a config item for this bundle type.
    $return = $settings[$entity->getEntityType()->id()][$entity->bundle()];

    // Optional base path.
    $return['base path'] = '';
    if (!empty($settings[$entity->getEntityType()->id()]['base path'])) {
      $return['base path'] = $settings[$entity->getEntityType()->id()]['base path'];
    }

    return $return;
  }

}
