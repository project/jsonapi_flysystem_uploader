JSON API Flysystem Uploader
===========================
Entities may have a JSON API representation automatically uploaded to S3 or
other file storage systems upon creation. This is handled using the Flysystem
module and library. Data will be stored in a file named "[uuid].json" based
upon the entity's UUID value.

The upload process is handled using the core Drupal queue system. When an
entity is created or updated an item will be added to the
"jsonapi_flysystem_uploader_push" queue. The next time that the website's cron
event runs it will process each queued item. Additionally, Drush (or other
tools) may be used to manually process items in the queue.

Configuration of the JSON output itself may be made using the JSON API Extras
module.
* https://www.drupal.org/project/jsonapi_extras

This module was designed to be as barebones as possible, so there currently
isn't even a way of editing the configuration through the UI. Currently the
configuration is all managed through the site's settings.php file, similar to
Flysystem; see below for details.


Requirements
--------------------------------------------------------------------------------
The following modules are required:

* JSON API
  https://www.drupal.org/project/jsonapi
  Requires at least v8.x-3.3.

* Flysystem
  https://www.drupal.org/project/flysystem

There are a variety of Flysystem submodules which add support for different
destination types, and more can be easily added using its flexible APIs.


Configuration
--------------------------------------------------------------------------------
In order to use this for a given entity type bundle, e.g. a specific content
type or vocabulary, some configuration must be added to the site's settings.php
file. The purpose of these lines are to inform the system which entity types
and entity bundles are to be processed.

// JSON API Flysystem Uploader.
// The ENTITYTYPE specifies the supported entity type, e.g. "node".
$settings['jsonapi_flysystem_uploader']['ENTITYTYPE'] = [
  // Optionally store all files in a subdirectory of this base path. Defaults to
  // an empty value, i.e. no base path will be used. All leading and trailing
  // slashes will be ignored.
  // Note: this means it is not possible to support an entity bundle with the
  // name "base path", but this should not be a problem.
  'base path' => '',

  // The configuration for a specific entity bundle, e.g. "article".
  'BUNDLENAME' => [
    // The name of the Flysystem configuration that is to be used, e.g. "s3".
    // These must be defined elsewhere.
    // @see $settings['flysystem']
    'flysystem' => 'SERVICENAME',

    // All files will be stored in a directory specified here. If this is
    // not specified then the file will be stored in a directory with the
    // same name as the bundle. Leading or trailing slashes are not needed
    // and will be ignored.
    'directory' => '',
  ],
];


Credits / contact
--------------------------------------------------------------------------------
Created and maintained by Damien McKenna [1].

Ongoing development is sponsored by Mediacurrent [2].

The best way to contact the authors is to submit an issue, be it a support
request, a feature request or a bug report, in the project issue queue:
  https://www.drupal.org/project/issues/jsonapi_flysystem_uploader


References
--------------------------------------------------------------------------------
1: https://www.drupal.org/u/damienmckenna
2: https://www.mediacurrent.com/
